Extended History aims to help module developers (or PHP-savvy web developers) by keeping an extended history of visits to nodes for each user.

While Drupal already provides some basic funcionality in the <em>history</em> table, there are a couple of drawbacks:

<ul>
	<li>History records are purged after 30 days;</li>
	<li>That 30-day limit is non-manageable, unless you disable cron runs altogether;</li>
	<li>It doesn't count how many times each user say that node.</li>
</li>

<code>
	db_query('DELETE FROM {history} WHERE timestamp < %d', NODE_NEW_LIMIT);
</code>

Extended History adds a new table called <em>extendedhistory</em> to store <em>(uid, nid, timestamp, count)</em> tuples.

For anonymous users, it keeps these details in session until they log in or register, at which time this is saved into the DB under their new UIDs.

If you want records to be deleted after some time, this can be set in Site Building » Extended History.

As 'API', it provides the following function:

<code>
	/**
	 * Returns an array of the form:
	 * array(
	 *   'timestamp' => [int],
	 *   'count'     => [int],
	 * );
	 * If no record was found, it returns an empty array.
	 */
	function extendedhistory_last_viewed()
</code>

Also, Extended History provides 1 block called 'You recently visited'. Please note that this block is only meant for registered users and node-like pages. If you need to take anonymous users into account and/or show the history for other pages, there are other great modules that do exactly that.
